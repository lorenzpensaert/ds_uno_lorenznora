/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

public enum ColorType {
    // RED = 0, YELLOW = 1, BLUE = 2, GREEN = 3, UNCOLORED = 4
    RED(0), YELLOW(1), BLUE(2), GREEN(3), UNCOLORED(4), NONE(-1);
    
    private final int value;
    private ColorType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ColorType getEnum(int value){
        for (ColorType e:ColorType.values()) {
            if(e.getValue() == value)
                return e;
        }
        return ColorType.NONE;//For values out of enum scope
    }
}
