/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import javax.swing.ImageIcon;

public interface Database extends Remote {
    // Account
    boolean register(String name, String password, boolean share) throws RemoteException;
    Player login(String name, String password) throws RemoteException;
    boolean isSessionTokenValid(int playerID) throws RemoteException;
    boolean logout(int playerID) throws RemoteException;
    
    // Lobby                
    int createGame(String name, int players, boolean share) throws RemoteException;
    boolean joinGame(int gameID, int playerID, boolean share) throws RemoteException;
    
    // Gameplay        
    List<Card> getCards() throws RemoteException;
    Card getCard(int type, int color, int value) throws RemoteException;
    Player getPlayer(int playerID) throws RemoteException;
    boolean setScore(int playerID, int gameID, int score, boolean win, boolean share) throws RemoteException;
    List<HighScore> getHighScores() throws RemoteException;
    ImageIcon getBackground() throws RemoteException;
}

