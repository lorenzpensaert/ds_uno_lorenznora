/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import javax.swing.ImageIcon;

public interface Application extends Remote {
     // Account
    boolean register(String name, String password) throws RemoteException;
    Player login(String name, String password) throws RemoteException;
    boolean isSessionTokenValid(int playerID) throws RemoteException;
    boolean logout(int playerID) throws RemoteException;
    
    // Lobby                
    int createGame(String name, int players) throws RemoteException;
    boolean joinGame(int gameID, Player player) throws RemoteException;
    boolean leaveGame(int gameID, int playerID) throws RemoteException;  
    boolean setGameState(int gameID, boolean running) throws RemoteException;  
    List<Player> getGamePlayers(int gameID) throws RemoteException;
    
    List<Game> getActiveGames() throws RemoteException;       
    boolean waitForPlayers(int gameID) throws RemoteException, InterruptedException;
    
    // Gameplay   
    Card getCard(int type, int color, int value) throws RemoteException;
    boolean checkCard(int gameID, Card card) throws RemoteException;
    void playCard(int gameID, int playerID, Card card) throws RemoteException;
    void endTurn(int gameID) throws RemoteException;
    Card drawCard(int gameID, int playerID) throws RemoteException;
    void drawInitialCards(int gameID, int playerID) throws RemoteException;
    List<String> getPlayerCards(int gameID, int playerID) throws RemoteException;
    boolean waitForTurn(int gameID, int playerID) throws RemoteException, InterruptedException;
    boolean waitGameChanged(int gameID) throws RemoteException, InterruptedException;
    Player getPlayer(int gameID, int playerID) throws RemoteException;
    Card getLastCard(int gameID) throws RemoteException;
    List<Card> getCards() throws RemoteException;
    boolean setScore(int playerID, int gameID, int score, boolean win) throws RemoteException;
    List<HighScore> getHighscores() throws RemoteException;
    ImageIcon getBackground() throws RemoteException;
}
