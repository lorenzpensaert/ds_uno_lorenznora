/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

public enum CardType {
    //NUMBER = 0, ADD = 1, ROTATE = 2, SKIP = 3, COLOR = 4
    NUMBER(0), ADD(1), ROTATE(2), SKIP(3), COLOR(4),NONE(-1);
    
    private final int value;
    private CardType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static CardType getEnum(int value){
        for (CardType e:CardType.values()) {
            if(e.getValue() == value)
                return e;
        }
        return CardType.NONE;//For values out of enum scope
    }
}
