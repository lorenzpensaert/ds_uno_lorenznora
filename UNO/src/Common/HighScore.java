/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.io.Serializable;

public class HighScore implements Serializable {
    private Player player;
    private int score;
    
    public HighScore(Player player, int score) {
        this.player = player;
        this.score = score;
    }

    public Player getPlayer() {
        return player;
    }

    public int getScore() {
        return score;
    }

    @Override
    public String toString() {
        if(player == null) return null;
        return "Name: " + player.getUsername() + " | Score=" + score;
    }     
}
