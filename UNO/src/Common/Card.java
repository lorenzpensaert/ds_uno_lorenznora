/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.io.Serializable;
import java.util.Objects;

public class Card implements Serializable{
    private int ID;
    private int value;
    private CardType type;
    private ColorType color;

    public Card(int ID, CardType type, ColorType color, int value) {
        this.ID = ID;
        this.value = value;
        this.type = type;
        this.color = color;
    }

    public int getID() {
        return ID;
    }
    
    public int getValue() {
        return value;
    }
        
    public int getColor() {
        return color.getValue();
    }
        
    public ColorType getColorEnum() {
        return color;
    }
    
    public int getType() {
        return type.getValue();
    }

    public CardType getTypeEnum(){
        return this.type;
    }
        
    public void setColor(ColorType color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return type.getValue() + "-" + color.getValue() + "-" + value;
    }   
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.ID;
        hash = 37 * hash + this.value;
        hash = 37 * hash + Objects.hashCode(this.type);
        hash = 37 * hash + Objects.hashCode(this.color);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Card other = (Card) obj;
        if (this.value != other.value) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if (this.color != other.color) {
            return false;
        }
        return true;
    }
    
}
