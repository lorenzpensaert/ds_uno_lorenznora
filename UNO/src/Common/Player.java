/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Player implements Serializable{
    private int ID;
    private String username;
    private List<Card> cardsInHand;
    private int score;

    public Player(int ID, String username) {
        this.ID = ID;
        this.username = username;
        this.cardsInHand = new ArrayList<>();
        this.score = 0;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Card> getCardsInHand() {
        return cardsInHand;
    }
    
    public int getCardsInHandCount() {
        return cardsInHand.size();
    }
    
    public void removeCardFromHand(Card c) {
        cardsInHand.remove(c);
    }
    
    public void addCardToHand(Card c) {
        cardsInHand.add(c);
    }

    @Override
    public String toString() {
        return username + ": " + cardsInHand.size() + " cards left | Score:" + score;
    }   

    public void setScore(int score) {
        this.score = score;
    }
}
