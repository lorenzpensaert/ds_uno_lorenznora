/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

/**
 *
 * @author lorenzpensaert
 */
public class Config {
    public static final int DISPATCH_PORT = 1200;
    public static final String DISPATCH_IP = "localhost";
    public static final String DISPATCH_NAME = "DispatcherService";
    
    public static final int DATABASE_PORTBASE = 1201;
    public static final int DATABASE_MAX = 3; // Don't put higher than APPLICATION_PORTBASE - DATABASE_PORTBASE!!!
    public static final String DATABASE_IP = "localhost";
    public static final String DATABASE_NAME = "DatabaseService";
    
    public static final int APPLICATION_PORTBASE = 1300;
    public static final int APPLICATION_MAX_PLAYERS = 20;
    public static final String APPLICATION_IP = "localhost";
    public static final String APPLICATION_NAME = "ApplicationService";
}
