/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import Servers.*;
import java.util.HashMap;

/**
 *
 * @author lorenzpensaert
 */
public interface Dispatcher extends Remote {
    boolean joinServer(int appID) throws RemoteException;
    void leaveServer(int appID) throws RemoteException;
    ArrayList<ApplicationServer> getServers() throws RemoteException;
    ArrayList<Integer> getDbPorts() throws RemoteException;
}
