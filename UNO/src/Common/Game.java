/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.io.Serializable;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Game implements Serializable{
    private String name;
    private int ID, playerCount, playerSelector;
    private boolean running, clockwiseTurns, gameChanged;
    private ArrayList<Player> players;
    private Stack<Card> playedCards, nonPlayedCards;
    
    public Game(int ID, String name, int playerCount, boolean running) {
        this.ID = ID;
        this.name = name;
        this.playerCount = playerCount;
        this.running = running;
        this.players = new ArrayList<>();
        this.playedCards = new Stack<>();
        this.nonPlayedCards = new Stack<>();
        this.playerSelector = 0;
        this.clockwiseTurns = true;
    }
    
    public void playCard(int dbPort, int playerID, Card card) {
        //remove the played card from the player's hand and add it to the played cards
        getPlayer(playerID).removeCardFromHand(card);
        playedCards.add(card);
        //rotate the playorder of the game
        if(card.getTypeEnum() == CardType.ROTATE)
            clockwiseTurns = !clockwiseTurns;
        //go to the next player in line
        if(card.getTypeEnum() == CardType.SKIP)
            getNextTurn();
        //go to the next player and add the correct amount of cards to his hand
        if(card.getTypeEnum() == CardType.ADD){
            getNextTurn();
            for(int i = 0; i < card.getValue(); i++) 
                drawCard(dbPort, getCurrentPlayer());
        }
        gameChanged = true;
    }
    
    public boolean hasGameChanged() {
        return gameChanged;
    }
    
    public void setGameChanged() {
        gameChanged = false;
    }
    
    public void getNextTurn() {        
        if(clockwiseTurns) playerSelector++;
        else playerSelector--;
        
        if(playerSelector < 0) playerSelector = playerCount - 1;
        else if(playerSelector > playerCount - 1) playerSelector = 0;
    } 
    
    public int getCurrentPlayer() {
        try {
            if(players.isEmpty()) return -1;
            return players.get(playerSelector).getID();
        } catch (Exception e) {
            getNextTurn();
            return players.get(playerSelector).getID();            
        }
    }

    public Stack<Card> getPlayedCards() {
        return playedCards;
    }
    
    public Card getLastPlayedCard() {
        try {
            return playedCards.peek();            
        } catch (Exception e) {
            return null;
        }
    }
    
    public boolean addPlayer(Player player){
        if(players.size() < playerCount && !players.contains(player)){
            players.add(player);
            return true;
        } else return false;
    }
    
    public boolean removePlayer(int playerID) {
        Player playerToRemove = null;
        for(Player p : players) {
            if(p.getID() == playerID) {
                playerToRemove = p;
            }
        }
        
        if(playerToRemove == null) return false;
        players.remove(playerToRemove);
        playerCount--;
        if(players.size() <= 0)
            running = false;
        return true;
    }
    
    public Card popCardFromNonPlayedCards(int dbPort) {
        if(nonPlayedCards.size() == 0) {
            try {
                Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, dbPort);
                Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
                for(Card c : impl.getCards()) {
                    nonPlayedCards.push(c);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return nonPlayedCards.pop();
    }
    
    public Card drawCard(int dbPort, int playerID) {  
        Card c = null;
        for(Player p : players) {
            if(p.getID() == playerID){
                c = popCardFromNonPlayedCards(dbPort);
                p.addCardToHand(c);
                return c;
            }
        }
        return c;
    }
    
    public List<Card> getPlayerCards(int playerID) {
        List<Card> cards = null;
        for(Player p : players) {
            if(p.getID() == playerID) {
                cards = p.getCardsInHand();
                break;
            }
        }
        return cards;
    }
    
    public int GetID(){
        return ID;
    }
    
    public String getName() {
        return name;
    }
    
    public Player getPlayer(int playerID) {        
        for(Player p : players) {
            if(p.getID() == playerID)
                return p;
        }
        return null;
    }
    
    public int getPlayerCount() {
        return playerCount;
    }
    
    public int getPlayersJoined() {
        return players.size();
    }
    
    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }   

    @Override
    public String toString() {
        return name + " | " + players.size() + "/" + playerCount;
    }        
}
