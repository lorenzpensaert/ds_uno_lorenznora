/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import Common.Application;
import Common.Config;
import Common.Player;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class LoginGUI extends javax.swing.JFrame {
    private static MessageDigest md;
    private int port;

    /**
     * Creates new form Login
     */
    public LoginGUI(int port) {
        initComponents();
        this.port = port;
        setVisible(true);
    }

    /**
     * Sends username and encrypted password to the application server to add user
     * Log this user in if register succeeded
     * @param username
     * @param password 
     */
    private void Register(String username, String password){
        try {
            password = cryptWithMD5(password);
            //send username and password to applicationserver to create user
            //If username correct, login with the same credentials
            Registry myRegistry = LocateRegistry.getRegistry(Config.APPLICATION_IP, port);
            Application impl = (Application) myRegistry.lookup(Config.APPLICATION_NAME);
            if(impl.register(username, password)){
                Player player = impl.login(username, password);
                if (player != null) {
                    // Geen idee wat te doen met de session token op dit moment, is ook niet geimplementeerd dus gaat een error geven, zet in commentaar om op te lossen.
                    //String sessionToken = impl.getSessionToken(playerID);
                    // Opens lobby and closes this form
                    LobbyGUI lobby = new LobbyGUI(port, player);
                    this.dispose();
                } else {
                    JOptionPane.showConfirmDialog(null,
                            "Wrong credentials!",
                            "Your username and/or password are wrong",
                            JOptionPane.OK_OPTION,
                            JOptionPane.ERROR_MESSAGE);
                }
            }else {
                JOptionPane.showConfirmDialog(null,
                        "Wrong credentials!",
                        "This username already exists!",
                        JOptionPane.OK_OPTION,
                        JOptionPane.ERROR_MESSAGE); 
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Log user in with given username and encrypted password
     * @param username
     * @param password 
     */
    private void Login(String username, String password){       
        try {
            password = cryptWithMD5(password);
            //send username and hashed password to applicationserver to check
            Registry myRegistry = LocateRegistry.getRegistry(Config.APPLICATION_IP, port);
            Application impl = (Application) myRegistry.lookup(Config.APPLICATION_NAME);
            Player player = impl.login(username, password);
            if(player != null) {                
                //TODO: Geen idee wat te doen met de session token op dit moment, is ook niet geimplementeerd dus gaat een error geven, zet in commentaar om op te lossen.
                //String sessionToken = impl.getSessionToken(playerID);
                // Opens lobby and closes this form
                LobbyGUI lobby = new LobbyGUI(port, player);
                this.dispose();
            } else {
                JOptionPane.showConfirmDialog(null,
                        "Wrong credentials!",
                        "Your username and/or password are wrong",
                        JOptionPane.OK_OPTION,
                        JOptionPane.ERROR_MESSAGE); 
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Encrypts given password with MD5 algorithm
     * @param password
     * @return 
     */
    public static String cryptWithMD5(String password){
        try {
            md = MessageDigest.getInstance("MD5");
            byte[] passBytes = password.getBytes();
            md.reset();
            byte[] digested = md.digest(passBytes);
            StringBuffer sb = new StringBuffer();
            for(int i=0;i<digested.length;i++){
                sb.append(Integer.toHexString(0xff & digested[i]));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(LoginGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
            return null;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnLogin = new javax.swing.JButton();
        btnRegisterLogin = new javax.swing.JButton();
        txtUsername = new javax.swing.JTextField();
        txtPassword = new javax.swing.JPasswordField();
        lblName1 = new javax.swing.JLabel();
        lblName2 = new javax.swing.JLabel();
        background = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(410, 150));
        setMinimumSize(new java.awt.Dimension(410, 150));
        setSize(new java.awt.Dimension(410, 150));
        getContentPane().setLayout(null);

        btnLogin.setBackground(new java.awt.Color(234, 219, 187));
        btnLogin.setText("Login");
        btnLogin.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(97, 69, 4)));
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });
        getContentPane().add(btnLogin);
        btnLogin.setBounds(10, 70, 170, 25);

        btnRegisterLogin.setBackground(new java.awt.Color(234, 219, 187));
        btnRegisterLogin.setText("Register & Login");
        btnRegisterLogin.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(97, 69, 4)));
        btnRegisterLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterLoginActionPerformed(evt);
            }
        });
        getContentPane().add(btnRegisterLogin);
        btnRegisterLogin.setBounds(190, 70, 170, 25);

        txtUsername.setBackground(new java.awt.Color(234, 219, 187));
        txtUsername.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(97, 69, 4)));
        getContentPane().add(txtUsername);
        txtUsername.setBounds(120, 10, 240, 25);

        txtPassword.setBackground(new java.awt.Color(234, 219, 187));
        txtPassword.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(97, 69, 4)));
        getContentPane().add(txtPassword);
        txtPassword.setBounds(120, 40, 240, 25);

        lblName1.setForeground(new java.awt.Color(234, 219, 187));
        lblName1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblName1.setText("Name:");
        getContentPane().add(lblName1);
        lblName1.setBounds(10, 10, 100, 25);

        lblName2.setForeground(new java.awt.Color(234, 219, 187));
        lblName2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblName2.setText("Password");
        getContentPane().add(lblName2);
        lblName2.setBounds(10, 40, 100, 25);

        background.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Client/tablebg.jpg"))); // NOI18N
        background.setText("jLabel1");
        getContentPane().add(background);
        background.setBounds(0, 0, 410, 150);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRegisterLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterLoginActionPerformed
        String username = txtUsername.getText();
        String password = String.valueOf(txtPassword.getPassword());
        Register(username, password);
    }//GEN-LAST:event_btnRegisterLoginActionPerformed

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        String username = txtUsername.getText();
        String password = String.valueOf(txtPassword.getPassword());
        Login(username, password);
    }//GEN-LAST:event_btnLoginActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel background;
    private javax.swing.JButton btnLogin;
    private javax.swing.JButton btnRegisterLogin;
    private javax.swing.JLabel lblName1;
    private javax.swing.JLabel lblName2;
    private javax.swing.JPasswordField txtPassword;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables
}
