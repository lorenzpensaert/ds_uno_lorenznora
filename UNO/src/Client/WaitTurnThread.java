/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import Common.Application;
import java.rmi.RemoteException;

public class WaitTurnThread implements Runnable {
    private Application application;
    private int gameID, playerID;
    private GameGUI game;
    private boolean myTurn, guiUpdated;
    
    public WaitTurnThread(Application application, GameGUI game, int gameID, int playerID) {
        this.application = application;
        this.game = game;
        this.gameID = gameID;
        this.playerID = playerID;
        this.myTurn = false;
        this.guiUpdated = false;
    }
    
    public void setTurnFalse() {
        game.updateGUI();
        this.myTurn = false;
        this.guiUpdated = false;
        game.setGUITurn(myTurn);
    }

    @Override
    public void run() { 
        while(true)
            synchronized(application) {
                try {
                    myTurn = application.waitForTurn(gameID, playerID);
                    if(!guiUpdated) {
                        game.updateGUI();
                        guiUpdated = true;
                    }
                    game.setGUITurn(myTurn);
                } catch(InterruptedException ex) {
                    ex.printStackTrace();
                } catch(RemoteException ex) {
                    ex.printStackTrace();
                }
            }
    }
}
