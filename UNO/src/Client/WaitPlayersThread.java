/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import Common.Application;
import java.rmi.RemoteException;
import javax.swing.JLabel;

public class WaitPlayersThread implements Runnable {
    
    private Application application;
    private int gameID;
    private GameGUI game;
    private boolean stop = true;
    
    public WaitPlayersThread(Application application, GameGUI game, int gameID) {
        this.application = application;
        this.game = game;
        this.gameID = gameID;
    }

    @Override
    public void run() {
        synchronized(application) {
            try {
                application.waitForPlayers(gameID);
                game.startGame();
            } catch(InterruptedException ex) {
                ex.printStackTrace();
            } catch(RemoteException ex) {
                ex.printStackTrace();
            }
        }
    }
    
}
