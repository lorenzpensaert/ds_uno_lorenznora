/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import Common.Application;
import java.rmi.RemoteException;

/**
 *
 * @author lorenzpensaert
 */
public class WaitWatchThread implements Runnable {
    private Application application;
    private int gameID;
    private WatchGUI game;
    private boolean changed;
    
    public WaitWatchThread(Application application, WatchGUI game, int gameID) {
        this.application = application;
        this.game = game;
        this.gameID = gameID;
        this.changed = false;
    }

    @Override
    public void run() { 
        while(true)
            synchronized(application) {
                try {
                    changed = application.waitGameChanged(gameID);
                    if(changed) {
                        game.updateGUI();
                        changed = false;
                    }
                } catch(InterruptedException ex) {
                    ex.printStackTrace();
                } catch(RemoteException ex) {
                    ex.printStackTrace();
                }
            }
    }
}
