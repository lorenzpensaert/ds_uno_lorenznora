/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import Common.Application;
import Common.Config;
import Common.Dispatcher;
import Common.Game;
import Common.Player;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class LobbyGUI extends javax.swing.JFrame {
    private int port;
    private Player player;
    private DefaultListModel<Game> listMod = new DefaultListModel<>();

    /**
     * Creates new form Lobby
     * @param playerID
     */
    public LobbyGUI(int port, Player player) {
        this.port = port;
        this.player = player; 
        initComponents();
        lblWelcome.setText("Welcome " + player.getUsername());
        FillGameList();
        setVisible(true);
    }
    
    private void FillGameList(){
        try {            
            Registry myRegistry = LocateRegistry.getRegistry(Config.APPLICATION_IP, port);
            Application impl = (Application) myRegistry.lookup(Config.APPLICATION_NAME);
            
            listMod.clear();
            
            for(Game g : impl.getActiveGames()) {
                listMod.addElement(g);
            }
            
            if(!listMod.isEmpty())
                lstGames.setModel(listMod);
        } catch (NotBoundException | RemoteException e) {
            e.printStackTrace();
        }
    }
   
    /**
     * Sends gamename and total players to applicationserver to create game
     * @param players
     * @param gameName 
     */
    private void CreateGame(int players, String gameName){
        try {
            //send #players to applicationserver to create game
            Registry myRegistry = LocateRegistry.getRegistry(Config.APPLICATION_IP, port);
            Application impl = (Application) myRegistry.lookup(Config.APPLICATION_NAME);
            int gameID = impl.createGame(gameName, players);
            if(!impl.joinGame(gameID, player))
                JOptionPane.showConfirmDialog(null,
                        "There are already enough players for this game",
                        "Game full",
                        JOptionPane.OK_OPTION,
                        JOptionPane.ERROR_MESSAGE);
            else {
                GameGUI game = new GameGUI(port, player, gameID);          
                this.dispose();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Sends player to application server and gameID to join this game
     * @param gameID 
     */
    private void JoinGame(int gameID){
        try {
            //send gameID and playerID to applicationserver to add player to the game
            Registry myRegistry = LocateRegistry.getRegistry(Config.APPLICATION_IP, port);
            Application impl = (Application) myRegistry.lookup(Config.APPLICATION_NAME);
            if(!impl.joinGame(gameID, player))
                JOptionPane.showConfirmDialog(null,
                        "There are already enough players for this game",
                        "Game full",
                        JOptionPane.OK_OPTION,
                        JOptionPane.ERROR_MESSAGE);
            else if(!impl.isSessionTokenValid(player.getID())) {
                JOptionPane.showConfirmDialog(null,
                        "Your sessiontoken has expired, please relog",
                        "Session token",
                        JOptionPane.OK_OPTION,
                        JOptionPane.ERROR_MESSAGE);
            } else {                
                GameGUI game = new GameGUI(port, player, gameID);
                this.dispose();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void WatchGame(int gameID) {
        try {
            //send gameID and playerID to applicationserver to add player to the game
            Registry myRegistry = LocateRegistry.getRegistry(Config.APPLICATION_IP, port);
            Application impl = (Application) myRegistry.lookup(Config.APPLICATION_NAME);
            if (!impl.isSessionTokenValid(player.getID())) {
                JOptionPane.showConfirmDialog(null,
                        "Your sessiontoken has expired, please relog",
                        "Session token",
                        JOptionPane.OK_OPTION,
                        JOptionPane.ERROR_MESSAGE);
            } else {
                WatchGUI game = new WatchGUI(port, gameID);
                this.dispose();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnLogout = new javax.swing.JButton();
        btnHighscores = new javax.swing.JButton();
        btnPlay = new javax.swing.JButton();
        btnWatch = new javax.swing.JButton();
        btnCreate = new javax.swing.JButton();
        txtGameName = new javax.swing.JTextField();
        txtPlayers = new javax.swing.JSpinner();
        lblWelcome = new javax.swing.JLabel();
        btnRefresh = new javax.swing.JButton();
        scrGames = new javax.swing.JScrollPane();
        lstGames = new javax.swing.JList<>();
        background = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(500, 420));
        setMinimumSize(new java.awt.Dimension(500, 420));
        setPreferredSize(new java.awt.Dimension(500, 420));
        getContentPane().setLayout(null);

        btnLogout.setBackground(new java.awt.Color(234, 219, 187));
        btnLogout.setForeground(new java.awt.Color(97, 69, 4));
        btnLogout.setText("Logout");
        btnLogout.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(97, 69, 4)));
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });
        getContentPane().add(btnLogout);
        btnLogout.setBounds(280, 42, 210, 25);

        btnHighscores.setBackground(new java.awt.Color(234, 219, 187));
        btnHighscores.setForeground(new java.awt.Color(97, 69, 4));
        btnHighscores.setText("Highscores");
        btnHighscores.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(97, 69, 4)));
        btnHighscores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHighscoresActionPerformed(evt);
            }
        });
        getContentPane().add(btnHighscores);
        btnHighscores.setBounds(280, 80, 210, 25);

        btnPlay.setBackground(new java.awt.Color(234, 219, 187));
        btnPlay.setForeground(new java.awt.Color(97, 69, 4));
        btnPlay.setText("Play game");
        btnPlay.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(97, 69, 4)));
        btnPlay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlayActionPerformed(evt);
            }
        });
        getContentPane().add(btnPlay);
        btnPlay.setBounds(6, 332, 176, 25);

        btnWatch.setBackground(new java.awt.Color(234, 219, 187));
        btnWatch.setForeground(new java.awt.Color(97, 69, 4));
        btnWatch.setText("Watch");
        btnWatch.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(97, 69, 4)));
        btnWatch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnWatchActionPerformed(evt);
            }
        });
        getContentPane().add(btnWatch);
        btnWatch.setBounds(188, 332, 84, 25);

        btnCreate.setBackground(new java.awt.Color(234, 219, 187));
        btnCreate.setForeground(new java.awt.Color(97, 69, 4));
        btnCreate.setText("Create");
        btnCreate.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(97, 69, 4)));
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });
        getContentPane().add(btnCreate);
        btnCreate.setBounds(188, 367, 84, 25);

        txtGameName.setBackground(new java.awt.Color(234, 219, 187));
        txtGameName.setForeground(new java.awt.Color(97, 69, 4));
        getContentPane().add(txtGameName);
        txtGameName.setBounds(4, 365, 140, 30);

        txtPlayers.setModel(new javax.swing.SpinnerNumberModel(2, 2, 4, 1));
        getContentPane().add(txtPlayers);
        txtPlayers.setBounds(145, 368, 37, 23);

        lblWelcome.setDisplayedMnemonic('s');
        lblWelcome.setForeground(new java.awt.Color(234, 219, 187));
        lblWelcome.setText("Welcome");
        getContentPane().add(lblWelcome);
        lblWelcome.setBounds(290, 10, 210, 16);

        btnRefresh.setBackground(new java.awt.Color(234, 219, 187));
        btnRefresh.setForeground(new java.awt.Color(97, 69, 4));
        btnRefresh.setText("Refresh");
        btnRefresh.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(97, 69, 4)));
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });
        getContentPane().add(btnRefresh);
        btnRefresh.setBounds(6, 6, 266, 25);

        lstGames.setBackground(new java.awt.Color(234, 219, 187));
        lstGames.setForeground(new java.awt.Color(97, 69, 4));
        scrGames.setViewportView(lstGames);

        getContentPane().add(scrGames);
        scrGames.setBounds(6, 41, 266, 285);

        background.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Client/tablebg.jpg"))); // NOI18N
        getContentPane().add(background);
        background.setBounds(0, 0, 500, 400);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
        if(txtGameName.getText().isEmpty()) {
             JOptionPane.showConfirmDialog(null,
                        "Please give your game a name",
                        "Create game",
                        JOptionPane.OK_OPTION,
                        JOptionPane.ERROR_MESSAGE);
        } else {
            int players = (int)txtPlayers.getValue();
            String gameName = txtGameName.getText();
            CreateGame(players, gameName);
        }
    }//GEN-LAST:event_btnCreateActionPerformed

    private void btnPlayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlayActionPerformed
        if(lstGames.getSelectedValue() == null) {
             JOptionPane.showConfirmDialog(null,
                        "Please select a game",
                        "Join game",
                        JOptionPane.OK_OPTION,
                        JOptionPane.ERROR_MESSAGE);
        } else {
            Game game = (Game) lstGames.getSelectedValue();        
            JoinGame(game.GetID());
        }
    }//GEN-LAST:event_btnPlayActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        FillGameList();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        try {
            Registry myRegistry = LocateRegistry.getRegistry(Config.APPLICATION_IP, port);
            Application impl = (Application) myRegistry.lookup(Config.APPLICATION_NAME);
            if (!impl.logout(player.getID())) {
                JOptionPane.showConfirmDialog(null,
                        "Can't log out!",
                        "Logout",
                        JOptionPane.OK_OPTION,
                        JOptionPane.ERROR_MESSAGE);
            } else {
                Registry reg = LocateRegistry.getRegistry(Config.DISPATCH_IP, Config.DISPATCH_PORT);
                Dispatcher imp = (Dispatcher) reg.lookup(Config.DISPATCH_NAME);
                imp.leaveServer(port);
                this.setVisible(false);
                this.dispose();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void btnHighscoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHighscoresActionPerformed
        HighScoreGUI hs = new HighScoreGUI(port);
        hs.setVisible(true);
    }//GEN-LAST:event_btnHighscoresActionPerformed

    private void btnWatchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnWatchActionPerformed
        if(lstGames.getSelectedValue() == null) {
             JOptionPane.showConfirmDialog(null,
                        "Please select a game",
                        "Join game",
                        JOptionPane.OK_OPTION,
                        JOptionPane.ERROR_MESSAGE);
        } else {
            Game game = (Game) lstGames.getSelectedValue();        
            WatchGame(game.GetID());
        }
    }//GEN-LAST:event_btnWatchActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel background;
    private javax.swing.JButton btnCreate;
    private javax.swing.JButton btnHighscores;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnPlay;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnWatch;
    private javax.swing.JLabel lblWelcome;
    private javax.swing.JList<Common.Game> lstGames;
    private javax.swing.JScrollPane scrGames;
    private javax.swing.JTextField txtGameName;
    private javax.swing.JSpinner txtPlayers;
    // End of variables declaration//GEN-END:variables
}
