/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;
import Common.ColorType;
import Common.Application;
import Common.Config;
import Common.Card;
import Common.CardType;
import Common.Player;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

public class GameGUI extends javax.swing.JFrame {

    private int gameID, port;
    private Player player;
    private Map<String, ImageIcon> imageMap;
    private Thread t,t2;
    private WaitTurnThread WTT;
    private Image img;
    private List<Card> allCards;
    /**
     * Creates new form Game
     */
    public GameGUI(int port, Player player, int gameID) {
        initComponents(); 
        this.port = port;
        this.player = player;
        this.gameID = gameID;
        
        waitForPlayers();
        
        try {
            Registry myRegistry = LocateRegistry.getRegistry(Config.APPLICATION_IP, port);
            Application impl = (Application) myRegistry.lookup(Config.APPLICATION_NAME);
            ImageIcon background = impl.getBackground();
            if(background == null)
                lblNextCard.setIcon(new ImageIcon(getClass().getResource("images_small/background.jpg")));
            else 
                lblNextCard.setIcon(background);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        lstCards.setCellRenderer(new CustomListRenderer());        
        lstCards.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        lstCards.setVisibleRowCount(1);
        lstCards.setLayoutOrientation(JList.HORIZONTAL_WRAP);
                
        JScrollPane scroll = new JScrollPane(lstCards);
        gridCards.add(scroll);
        this.pack();
        setVisible(true);
    }
    
    private void waitForPlayers(){
        try {
            Registry myRegistry = LocateRegistry.getRegistry(Config.APPLICATION_IP, port);
            Application impl = (Application) myRegistry.lookup(Config.APPLICATION_NAME);
            WaitPlayersThread WPT = new WaitPlayersThread(impl, this, gameID);
            t = new Thread(WPT);
            t.start();
            
            allCards = impl.getCards();
            String[] allCardsStr = new String[allCards.size()];
            for(int i = 0; i < allCards.size(); i++) {
                allCardsStr[i] = allCards.get(i).toString();
            }
            imageMap = createImageMap(allCardsStr);             
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startGame() {
        // Get everyone's cards
        try {
            Registry myRegistry = LocateRegistry.getRegistry(Config.APPLICATION_IP, port);
            Application impl = (Application) myRegistry.lookup(Config.APPLICATION_NAME);                                    
            impl.drawInitialCards(gameID, player.getID());            
            
            updateGUI();
            
            // Start waiting for turn
            WTT = new WaitTurnThread(impl, this, gameID, player.getID());
            t = new Thread(WTT);
            t.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    private void playCard(){
        try {
            if (lstCards.getSelectedValue() == null) {
                JOptionPane.showConfirmDialog(null, "Please select a card", "Wrong card", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE);
                return;
            }

            Registry myRegistry = LocateRegistry.getRegistry(Config.APPLICATION_IP, port);
            Application impl = (Application) myRegistry.lookup(Config.APPLICATION_NAME);
            String selectedCard = lstCards.getSelectedValue();
            String[] cardProp = selectedCard.split("-");
            
            Card playedCard = allCards.stream().filter(c -> c.getType() == Integer.parseInt(cardProp[0]) && c.getColor() == Integer.parseInt(cardProp[1]) && c.getValue() == Integer.parseInt(cardProp[2])).findFirst().get();
            
            if (impl.checkCard(gameID, playedCard)) {
                if(playedCard.getTypeEnum() == CardType.COLOR || (playedCard.getTypeEnum()== CardType.ADD && playedCard.getValue() == 4)) {
                    ColorType[] list = {ColorType.RED, ColorType.YELLOW, ColorType.BLUE, ColorType.GREEN};
                    JComboBox jcb = new JComboBox(list);
                    jcb.setEditable(true);
                    JOptionPane.showMessageDialog( null, jcb, "Choose a color for the next player", JOptionPane.QUESTION_MESSAGE);
                    playedCard.setColor((ColorType) jcb.getSelectedItem());
                    impl.playCard(gameID, player.getID(), playedCard);
                    List<String> myCards = impl.getPlayerCards(gameID, player.getID());                    
                    WTT.setTurnFalse(); 
                    if (myCards.isEmpty()) {
                        calculateScore();
                    }
                } else {
                    impl.playCard(gameID, player.getID(), playedCard);
                    List<String> myCards = impl.getPlayerCards(gameID, player.getID());
                    WTT.setTurnFalse(); 
                    if (myCards.isEmpty()) {
                        calculateScore();
                    }
                }
            } else {
                JOptionPane.showConfirmDialog(null, "This card can't be played", "Wrong card", JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void playCard(Application impl, Card playedCard){
        try {
            if (impl.checkCard(gameID, playedCard)) {
                if(playedCard.getTypeEnum() == CardType.COLOR || (playedCard.getTypeEnum()== CardType.ADD && playedCard.getValue() == 4)) {
                    ColorType[] list = {ColorType.RED, ColorType.YELLOW, ColorType.BLUE, ColorType.GREEN};
                    JComboBox jcb = new JComboBox(list);
                    jcb.setEditable(true);
                    JOptionPane.showMessageDialog( null, jcb, "Choose a color for the next player", JOptionPane.QUESTION_MESSAGE);
                    playedCard.setColor((ColorType) jcb.getSelectedItem());
                    impl.playCard(gameID, player.getID(), playedCard);
                    List<String> myCards = impl.getPlayerCards(gameID, player.getID());
                    if (myCards.isEmpty()) {
                        calculateScore();
                    }
                    WTT.setTurnFalse(); 
                } else {
                    impl.playCard(gameID, player.getID(), playedCard);
                    List<String> myCards = impl.getPlayerCards(gameID, player.getID());
                    if (myCards.isEmpty()) {
                        calculateScore();
                    }
                    WTT.setTurnFalse(); 
                }
            } else {
                impl.endTurn(gameID);
                WTT.setTurnFalse();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void drawCard() {
        try {                      
            Registry myRegistry = LocateRegistry.getRegistry(Config.APPLICATION_IP, port);
            Application impl = (Application) myRegistry.lookup(Config.APPLICATION_NAME);

            // Draw a card
            Card c = impl.drawCard(gameID, player.getID());
            updateGUI();            
            lstCards.setSelectedValue(c, true);
            playCard(impl, c);
            btnDrawCard.setEnabled(false);
        } catch (NotBoundException | RemoteException e) {
            e.printStackTrace();
        }
    }
            
    public void updateGUI() {        
        try {
            Registry myRegistry = LocateRegistry.getRegistry(Config.APPLICATION_IP, port);
            Application impl = (Application) myRegistry.lookup(Config.APPLICATION_NAME);
            Card lastCard = impl.getLastCard(gameID);
            
            if(lastCard != null) lblCurrentCard.setIcon(new ImageIcon(getClass().getResource("images_small/" + lastCard.toString() + ".jpg")));

            List<String> myCards = impl.getPlayerCards(gameID, player.getID());
            String[] tmp = new String[myCards.size()];
            myCards.toArray(tmp);
            lstCards.setListData(tmp);
            
            List<Player> players = impl.getGamePlayers(gameID);
            if (players.size() == 4) {
                lblPlayer4.setText(players.get(3).toString());
                lblPlayer1.setVisible(true);
                lblPlayer2.setVisible(true);
                lblPlayer3.setVisible(true);
                lblPlayer4.setVisible(true);
            } else if (players.size() == 3) {
                lblPlayer1.setText(players.get(0).toString());
                lblPlayer2.setText(players.get(1).toString());
                lblPlayer3.setText(players.get(2).toString());
                lblPlayer1.setVisible(true);
                lblPlayer2.setVisible(true);
                lblPlayer3.setVisible(true);
                lblPlayer4.setVisible(false);
            } else if (players.size() == 2) {
                lblPlayer1.setText(players.get(0).toString());
                lblPlayer2.setText(players.get(1).toString());
                lblPlayer1.setVisible(true);
                lblPlayer2.setVisible(true);
                lblPlayer3.setVisible(false);
                lblPlayer4.setVisible(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void calculateScore(){
        int score = 0;
        boolean win = false;
        try{
            Registry myRegistry = LocateRegistry.getRegistry(Config.APPLICATION_IP, port);
            Application impl = (Application) myRegistry.lookup(Config.APPLICATION_NAME);
            List<Player> players = impl.getGamePlayers(gameID);
            int playersTotal = players.size();
            int stillPlaying = 0;
            for(Player p : players){
                    List<String> cards = impl.getPlayerCards(gameID, p.getID());
                    if(cards.size() > 0) stillPlaying++;
                    for(String c : cards){
                        String[] card = c.split("-");
                        int type = Integer.parseInt(card[0]);
                        if(type == CardType.NUMBER.getValue())
                            score += Integer.parseInt(card[2]);
                        if(type == CardType.SKIP.getValue() || type == CardType.ROTATE.getValue() || (type == CardType.ADD.getValue() && Integer.parseInt(card[2]) == 2))
                            score += 20;
                        if(type == CardType.COLOR.getValue() || (type == CardType.ADD.getValue() && Integer.parseInt(card[2]) == 4))
                            score += 50;
                    }
            }
            if(stillPlaying == playersTotal - 1) win = true;
            
            if(impl.setScore(player.getID(), gameID, score, win)){                
                JOptionPane.showConfirmDialog(null, "Well played! Your score is: " + score, "Your score", JOptionPane.INFORMATION_MESSAGE);
                impl.leaveGame(gameID, player.getID());
                LobbyGUI lobby = new LobbyGUI(port, player);
                this.dispose();
            }

                
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void setGUITurn(boolean isMyTurn) {
        btnPlayCard.setEnabled(isMyTurn);
        btnDrawCard.setEnabled(isMyTurn);        
        lblPlayerTurn.setText(isMyTurn ? "It's your turn, play a card" : "Wait for your turn");
    }
    
    public class CustomListRenderer extends DefaultListCellRenderer {

        Font font = new Font("helvitica", Font.BOLD, 0);

        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {

            JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            label.setIcon(imageMap.get((String) value));
            label.setHorizontalTextPosition(JLabel.CENTER);
            label.setFont(font);
            return label;
        }
    }

    private Map<String, ImageIcon> createImageMap(String[] list) {
        Map<String, ImageIcon> map = new HashMap<>();
        for (String s : list) {
            map.put(s, new ImageIcon(getClass().getResource("images_small/" + s + ".jpg")));
        }
        return map;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        gridCards = new javax.swing.JPanel();
        lstCards = new javax.swing.JList<>();
        btnDrawCard = new javax.swing.JButton();
        btnPlayCard = new javax.swing.JButton();
        lblPlayer2 = new javax.swing.JLabel();
        lblPlayer3 = new javax.swing.JLabel();
        lblPlayer4 = new javax.swing.JLabel();
        lblPlayer1 = new javax.swing.JLabel();
        lblPlayerTurn = new javax.swing.JLabel();
        lblCurrentCard = new javax.swing.JLabel();
        lblNextCard = new javax.swing.JLabel();
        backgroundImage = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(1400, 690));
        setMinimumSize(new java.awt.Dimension(1400, 690));
        setSize(new java.awt.Dimension(1400, 690));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(null);

        gridCards.setBackground(new java.awt.Color(234, 219, 187));
        gridCards.setLayout(new java.awt.GridLayout(1, 7));

        lstCards.setBackground(new java.awt.Color(234, 219, 187));
        lstCards.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        lstCards.setToolTipText("");
        lstCards.setLayoutOrientation(javax.swing.JList.HORIZONTAL_WRAP);
        gridCards.add(lstCards);

        getContentPane().add(gridCards);
        gridCards.setBounds(0, 397, 1394, 280);

        btnDrawCard.setBackground(new java.awt.Color(234, 219, 187));
        btnDrawCard.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        btnDrawCard.setForeground(new java.awt.Color(97, 69, 4));
        btnDrawCard.setText("Draw Card");
        btnDrawCard.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(97, 69, 4)));
        btnDrawCard.setEnabled(false);
        btnDrawCard.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnDrawCard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDrawCardActionPerformed(evt);
            }
        });
        getContentPane().add(btnDrawCard);
        btnDrawCard.setBounds(512, 344, 370, 35);

        btnPlayCard.setBackground(new java.awt.Color(234, 219, 187));
        btnPlayCard.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        btnPlayCard.setForeground(new java.awt.Color(97, 69, 4));
        btnPlayCard.setText("Play Card");
        btnPlayCard.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(97, 69, 4)));
        btnPlayCard.setEnabled(false);
        btnPlayCard.setMargin(new java.awt.Insets(0, 0, 0, 0));
        btnPlayCard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlayCardActionPerformed(evt);
            }
        });
        getContentPane().add(btnPlayCard);
        btnPlayCard.setBounds(512, 303, 372, 35);

        lblPlayer2.setForeground(new java.awt.Color(234, 219, 187));
        lblPlayer2.setText("Player 2: X cards | Score:");
        getContentPane().add(lblPlayer2);
        lblPlayer2.setBounds(6, 191, 200, 50);

        lblPlayer3.setForeground(new java.awt.Color(234, 219, 187));
        lblPlayer3.setText("Player 3: X cards | Score:");
        getContentPane().add(lblPlayer3);
        lblPlayer3.setBounds(1188, 41, 200, 50);

        lblPlayer4.setForeground(new java.awt.Color(234, 219, 187));
        lblPlayer4.setText("Player 4: X cards | Score:");
        getContentPane().add(lblPlayer4);
        lblPlayer4.setBounds(1188, 191, 200, 50);

        lblPlayer1.setForeground(new java.awt.Color(234, 219, 187));
        lblPlayer1.setText("Player 1: X cards | Score:");
        getContentPane().add(lblPlayer1);
        lblPlayer1.setBounds(6, 41, 200, 50);

        lblPlayerTurn.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        lblPlayerTurn.setForeground(new java.awt.Color(234, 219, 187));
        lblPlayerTurn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPlayerTurn.setText("Waiting for players to join...");
        getContentPane().add(lblPlayerTurn);
        lblPlayerTurn.setBounds(512, 6, 372, 23);

        lblCurrentCard.setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().add(lblCurrentCard);
        lblCurrentCard.setBounds(512, 41, 180, 256);

        lblNextCard.setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().add(lblNextCard);
        lblNextCard.setBounds(704, 41, 180, 256);

        backgroundImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Client/tablebg.jpg"))); // NOI18N
        getContentPane().add(backgroundImage);
        backgroundImage.setBounds(0, 0, 1400, 690);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPlayCardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlayCardActionPerformed
        playCard();
    }//GEN-LAST:event_btnPlayCardActionPerformed

    private void btnDrawCardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDrawCardActionPerformed
        drawCard();
    }//GEN-LAST:event_btnDrawCardActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        LobbyGUI lobby = new LobbyGUI(port, player);
        this.dispose();
    }//GEN-LAST:event_formWindowClosing

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel backgroundImage;
    private javax.swing.JButton btnDrawCard;
    private javax.swing.JButton btnPlayCard;
    private javax.swing.JPanel gridCards;
    private javax.swing.JLabel lblCurrentCard;
    private javax.swing.JLabel lblNextCard;
    private javax.swing.JLabel lblPlayer1;
    private javax.swing.JLabel lblPlayer2;
    private javax.swing.JLabel lblPlayer3;
    private javax.swing.JLabel lblPlayer4;
    private javax.swing.JLabel lblPlayerTurn;
    private javax.swing.JList<String> lstCards;
    // End of variables declaration//GEN-END:variables
}


