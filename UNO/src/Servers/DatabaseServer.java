/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servers;

import java.io.File;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.*;
import java.util.Date;
import Common.Config;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 *
 * @author lorpo
 */
public class DatabaseServer {
    private int port;
    private String dbName;
    private DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
    
    public DatabaseServer(int port) {
        this.port = port;
        dbName = "UnoDatabase" + (port - Config.DATABASE_PORTBASE) + ".db";
        // Check if the database is available
        File f = new File(dbName);
        if (!(f.exists() && !f.isDirectory())) {
            CreateDB();
            Seed();
        }
        try {
            Registry registry = LocateRegistry.createRegistry(this.port);
            registry.rebind(Config.DATABASE_NAME, new DatabaseImpl(dbName, this.port));
            System.out.println("Database server is ready.");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Database server failed to start.");
        }
    }
    
    public int getPort() {
        return port;
    }
       
    private void Seed() {
        Connection c = null;
        Date date = new Date();
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbName);            
            for(int i = 0; i <= 3; i++) {
                // Add each number (0-9) in each color
                for(int j = 1; j <= 9; j++){
                    try (PreparedStatement query = c.prepareStatement("INSERT INTO CARDS (ID,TYPE,COLOR,VALUE,ROWVERSION) VALUES (NULL, ?, ?, ?, NULL)")) {
                        query.setInt(1, 0);     // Type = NUMBERS
                        query.setInt(2, i);     // Color = i
                        query.setInt(3, j);     // Value = j
                        query.executeUpdate();
                    }
                }       
                // Add 2x of each color of each Add / Rotate / Skip card
                for(int j = 0; j <= 1; j++) {
                    /*
                    try (PreparedStatement query = c.prepareStatement("INSERT INTO CARDS (ID,TYPE,COLOR,VALUE) VALUES (NULL, ?, ?, ?)")) {
                        query.setInt(1, 1);     // Type = ADD
                        query.setInt(2, i);     // Color = i
                        query.setInt(3, 2);     // Value = j
                        query.executeUpdate();
                    }
                    */
                    try (PreparedStatement query = c.prepareStatement("INSERT INTO CARDS (ID,TYPE,COLOR,VALUE,ROWVERSION) VALUES (NULL, ?, ?, ?, NULL)")) {
                        query.setInt(1, 2);     // Type = ROTATE
                        query.setInt(2, i);     // Color = i
                        query.setInt(3, 0);     // Value = j
                        query.executeUpdate();
                    }
                    try (PreparedStatement query = c.prepareStatement("INSERT INTO CARDS (ID,TYPE,COLOR,VALUE,ROWVERSION) VALUES (NULL, ?, ?, ?, NULL)")) {
                        query.setInt(1, 3);     // Type = SKIP
                        query.setInt(2, i);     // Color = i
                        query.setInt(3, 1);     // Value = j
                        query.executeUpdate();
                    }
                }
            }
            /*
            for(int i = 0; i <= 3; i++) {
                
                try (PreparedStatement query = c.prepareStatement("INSERT INTO CARDS (ID,TYPE,COLOR,VALUE) VALUES (NULL, ?, ?, ?)")) {
                    query.setInt(1, 1);     // Type = ADD
                    query.setInt(2, 4);     // Color = UNCOLORED
                    query.setInt(3, 4);     // Value = 4
                    query.executeUpdate();
                }
                
                try (PreparedStatement query = c.prepareStatement("INSERT INTO CARDS (ID,TYPE,COLOR,VALUE) VALUES (NULL, ?, ?, ?)")) {
                    query.setInt(1, 4);     // Type = COLOR
                    query.setInt(2, 4);     // Color = UNCOLORED
                    query.setInt(3, 0);     // Value = 0
                    query.executeUpdate();
                }
            }
            */  
            
            Date s = new Date(2017, 12, 1);
            String start = dateFormat.format(s);
            
            Date e = new Date(2017, 12, 30);
            String end = dateFormat.format(e);

            try (PreparedStatement query = c.prepareStatement("INSERT INTO BACKGROUNDS (ID,START,END,IMAGEPATH,ROWVERSION) VALUES (NULL, ?, ?, ?, NULL)")) {
                        query.setString(1, start);
                        query.setString(2, end);
                        query.setString(3, "background_christmas.jpg");
                        query.executeUpdate();
            }
            
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void CreateDB() {
        Connection c = null;
        Statement stmt = null;
      
        try {
           Class.forName("org.sqlite.JDBC");
           c = DriverManager.getConnection("jdbc:sqlite:" + dbName);

            System.out.println("Opened database successfully");
            stmt = c.createStatement();
            String sqlPlayer = "CREATE TABLE IF NOT EXISTS PLAYERS " +
                          "(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                          " USERNAME            TEXT        NOT NULL    UNIQUE, " + 
                          " PASSWORD            TEXT        NOT NULL, " + 
                          " SESSIONTOKENDATE    DATETIME, " +
                          " SESSIONTOKEN        TEXT, " +
                          " ROWVERSION          DATETIME DEFAULT CURRENT_TIMESTAMP)"; 
            stmt.executeUpdate(sqlPlayer);
            stmt.close();

            stmt = c.createStatement();
            String sqlCard = "CREATE TABLE IF NOT EXISTS CARDS " +
                          "(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                          " TYPE                INT        NOT NULL, " + 
                          " COLOR               INT         NOT NULL, " + 
                          " VALUE               INT        NOT NULL, " +
                          " ROWVERSION          DATETIME DEFAULT CURRENT_TIMESTAMP)"; 
            stmt.executeUpdate(sqlCard);
            stmt.close();

            stmt = c.createStatement();
            String sqlGame = "CREATE TABLE IF NOT EXISTS GAMES " +
                          "(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                          " NAME                TEXT        NOT NULL, " +
                          " PLAYERS             INT         NOT NULL, " + 
                          " RUNNING             BOOL     NOT NULL, " +
                          " ROWVERSION          DATETIME DEFAULT CURRENT_TIMESTAMP)"; 
            stmt.executeUpdate(sqlGame);
            stmt.close();

            stmt = c.createStatement();
            String sqlPlayers_Games = "CREATE TABLE IF NOT EXISTS PLAYERS_GAMES " +
                          "(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                          " PLAYER_ID           INT         NOT NULL, " + 
                          " GAME_ID             INT         NOT NULL, " + 
                          " SCORE               INT         NOT NULL, " + 
                          " LSTCARDS               TEXT        NOT NULL, " +
                          " ROWVERSION          DATETIME DEFAULT CURRENT_TIMESTAMP, " +
                          " FOREIGN KEY (PLAYER_ID)         REFERENCES PLAYERS(ID), " +
                          " FOREIGN KEY (GAME_ID)           REFERENCES GAMES(ID))"; 
            stmt.executeUpdate(sqlPlayers_Games);
            stmt.close();
            
            stmt = c.createStatement();
            String sqlBackground = "CREATE TABLE IF NOT EXISTS BACKGROUNDS " +
                          "(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                          " START               DATETIME        NOT NULL, " + 
                          " END                 DATETIME        NOT NULL, " + 
                          " IMAGEPATH           TEXT            NOT NULL, " +
                          " ROWVERSION          DATETIME DEFAULT CURRENT_TIMESTAMP)"; 
            stmt.executeUpdate(sqlBackground);
            stmt.close();

            stmt = c.createStatement();
            String sqlRanking = "CREATE TABLE IF NOT EXISTS RANKING " +
                          "(ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                          " PLAYER_ID           INT         NOT NULL, " + 
                          " SCORE               INT         NOT NULL, " + 
                          " WINS                INT         NOT NULL, " +
                          " ROWVERSION          DATETIME DEFAULT CURRENT_TIMESTAMP, " +
                          " FOREIGN KEY (PLAYER_ID)         REFERENCES PLAYERS(ID))"; 
            stmt.executeUpdate(sqlBackground);
            stmt.close();
            c.close();
        } catch ( ClassNotFoundException | SQLException e ) {
            e.printStackTrace();
        }
    }
}
