/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servers;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import Common.Card;
import Common.CardType;
import Common.ColorType;
import Common.Config;
import Common.Database;
import Common.Dispatcher;
import Common.HighScore;
import Common.Player;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Calendar;
import java.util.HashMap;
import javax.swing.ImageIcon;

public class DatabaseImpl extends UnicastRemoteObject implements Database {
    private Connection c;
    private String dbName;            
    private DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
    private int dbPort;

    public DatabaseImpl(String dbName, int dbPort) throws RemoteException {
        this.dbName = dbName;
        this.dbPort = dbPort;
    }

    private ArrayList<Integer> getDbPorts() {
        try {
            Registry myRegistry = LocateRegistry.getRegistry(Config.DISPATCH_IP, Config.DISPATCH_PORT);
            Dispatcher impl = (Dispatcher) myRegistry.lookup(Config.DISPATCH_NAME);
            return impl.getDbPorts();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
        
    @Override
    public boolean register(String name, String password, boolean share) throws RemoteException {
        try {
            if(share) {
                ArrayList<Integer> dbServers = getDbPorts();
                for(Integer db : dbServers){
                    if(dbPort != db) {
                        Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, db);
                        Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
                        impl.register(name, password, false);
                    }
                }
            }
            
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            
            Date rv = new Date();
            String rowversion = dateFormat.format(rv);
            
            try (PreparedStatement query = c.prepareStatement("INSERT INTO PLAYERS (ID,USERNAME,PASSWORD, SESSIONTOKENDATE, SESSIONTOKEN, ROWVERSION) VALUES (NULL, ?, ?, NULL, NULL, ?)")) {
                query.setString(1, name);
                query.setString(2, password);
                query.setString(3, rowversion);
                query.executeUpdate();
            }
            
            c.close();
            System.out.println("REGISTER: Succesfull");
            return true;
        } catch (Exception e) {
            System.out.println("REGISTER: Failed");
            e.printStackTrace();
            return false;
        } 
    }

    @Override
    public Player login(String name, String password) throws RemoteException {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            
            UUID sessionToken = UUID.randomUUID();
            Date sessionTokenDate = new Date();
            DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
            String sTD = dateFormat.format(sessionTokenDate);
            
            Date rv = new Date();
            String rowversion = dateFormat.format(rv);
            
            
            Player p = null;
            try (PreparedStatement query = c.prepareStatement("SELECT * FROM PLAYERS WHERE USERNAME == ? AND PASSWORD == ?")) {
                query.setString(1, name);
                query.setString(2, password);
                ResultSet rs = query.executeQuery();
                while (rs.next()) {
                    p = new Player(rs.getInt("ID"), rs.getString("USERNAME"));
                    Date test = new Date(rs.getString("ROWVERSION"));
                    if(test.before(rv)){
                        try (PreparedStatement updateSession = c.prepareStatement("UPDATE PLAYERS SET SESSIONTOKENDATE = ?, ROWVERSION = ?, SESSIONTOKEN = ? WHERE USERNAME = ? AND PASSWORD = ?")) {
                            updateSession.setString(1, sTD);
                            updateSession.setString(2, rowversion);
                            updateSession.setString(3, sessionToken.toString());
                            updateSession.setString(4, name);
                            updateSession.setString(5, password);
                            updateSession.executeUpdate();
                            //query.close();
                        }
                    }
                }     
                rs.close();
            }
            
            c.close();
            System.out.println("LOGIN: Succesfull");            
            return p;
        } catch ( ClassNotFoundException | SQLException e ) {
            System.out.println("LOGIN: Failed");
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ImageIcon getBackground() throws RemoteException {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            
            Date today = new Date(2017,12,19);
            String background = "";
            try (PreparedStatement query = c.prepareStatement("SELECT * FROM BACKGROUNDS")) {

                ResultSet rs = query.executeQuery();
                while (rs.next()) {
                    Date start = new Date(rs.getString("START"));
                    Date end = new Date(rs.getString("END"));
                    System.out.println(today);
                    if(start.before(today) && end.after(today)) {
                        background = rs.getString("IMAGEPATH");
                        System.out.println(rs.getString("IMAGEPATH"));
                    }
                }   
                rs.close();
            }
            c.close();

            if(background.equals("")) {
                System.out.println("GETBACKGROUND: No special dates found!");   
                return null;
            }
            System.out.println("GETBACKGROUND: Succesfull");     
            return new ImageIcon(getClass().getResource(background));
        } catch ( ClassNotFoundException | SQLException e ) {
            System.out.println("GETBACKGROUND: Failed");
            return null;
        }
    }
    
    @Override
    public boolean isSessionTokenValid(int playerID) throws RemoteException {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            boolean result = false;
            try (PreparedStatement query = c.prepareStatement("SELECT * FROM PLAYERS WHERE ID == ?")) {
                query.setInt(1, playerID);
                ResultSet rs = query.executeQuery();
                while (rs.next()) {
                    String dateStr = rs.getString("SESSIONTOKENDATE");
                    String sessionToken = rs.getString("SESSIONTOKEN");
                    DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
                    try {
                        Date compareDate = new Date();
                        Date date = dateFormat.parse(dateStr);
                        long diff = compareDate.getTime() - date.getTime();
                        
                        if(TimeUnit.HOURS.convert(diff, TimeUnit.MILLISECONDS) <= 24)
                            result = true;
                    } catch (ParseException ex) {
                        Logger.getLogger(DatabaseImpl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }   
                rs.close();
            }
                        
            c.close();
            System.out.println("ISSESSIONTOKENVALID: Succesfull");       
            return result;
        } catch ( ClassNotFoundException | SQLException e ) {
            System.out.println("ISSESSIONTOKENVALID: Failed");
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean logout(int playerID) throws RemoteException {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            
            try(PreparedStatement select = c.prepareStatement("SELECT * FROM PLAYERS WHERE ID == ?")){
                select.setInt(1, playerID);
                ResultSet rs = select.executeQuery();
                while (rs.next()) {
                    Player p = new Player(rs.getInt("ID"), rs.getString("USERNAME"));
                    try (PreparedStatement query = c.prepareStatement("UPDATE PLAYERS SET SESSIONTOKENDATE = NULL, SESSIONTOKEN = NULL WHERE ID = ?")) {
                        query.setInt(1, playerID);
                        query.executeUpdate();
                    }
                }   
                rs.close();
            }
           
            c.close();
            System.out.println("LOGOUT: Succesfull");            
            return true;
        } catch ( ClassNotFoundException | SQLException e ) {
            System.out.println("LOGOUT: Failed");
            e.printStackTrace();
            return false;
        }    
    }

    @Override
    public int createGame(String name, int players, boolean share) throws RemoteException {
        try {
            if(share) {
                ArrayList<Integer> dbServers = getDbPorts();
                for(Integer db : dbServers){
                    if(dbPort != db) {
                        Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, db);
                        Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
                        impl.createGame(name, players, false);
                    }
                }
            }
            
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            
            Date rv = new Date();
            String rowversion = dateFormat.format(rv);
            
            int id;            
            try (PreparedStatement query = c.prepareStatement("INSERT INTO GAMES (ID,NAME,PLAYERS,RUNNING,ROWVERSION) VALUES (NULL, ?, ?, 1, ?);")) {
                query.setString(1, name);
                query.setInt(2, players);
                query.setString(3, rowversion);
                query.executeUpdate();
                ResultSet rs = query.getGeneratedKeys();
                id = -1;
                while(rs.next()) {
                    id = rs.getInt(1);
                } 
                rs.close();
            }
            
            c.close();
            System.out.println("CREATE GAME: Succesfull");
            return id;
        } catch ( Exception e ) {
            System.out.println("CREATE GAME: Failed");
            return -1;
        } 
    }

    @Override
    public boolean joinGame(int gameID, int playerID, boolean share) throws RemoteException {
        try {
            if(share) {
                ArrayList<Integer> dbServers = getDbPorts();
                for(Integer db : dbServers){
                    if(dbPort != db) {
                        Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, db);
                        Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
                        impl.joinGame(gameID, playerID, false);
                    }
                }
            }
            
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            
            Date rv = new Date();
            String rowversion = dateFormat.format(rv);
            
            int id;            
            try (PreparedStatement query = c.prepareStatement("INSERT INTO PLAYERS_GAMES (ID, PLAYER_ID, GAME_ID, LSTCARDS, SCORE, ROWVERSION) VALUES (NULL, ?, ?, ?, 0, ?);")) {
                query.setInt(1, playerID);
                query.setInt(2, gameID);
                query.setString(3, "");
                query.setString(4, rowversion);
                query.executeUpdate();
            }
            
            c.close();
            System.out.println("JOIN GAME: Succesfull");
            return true;
        } catch ( Exception e ) {
            System.out.println("JOIN GAME: Failed");
            return false;
        } 
    }

    @Override
    public List<Card> getCards() throws RemoteException {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            
            List<Card> cards = new ArrayList<>();
            try (PreparedStatement query = c.prepareStatement("SELECT * FROM CARDS")) {
                ResultSet rs = query.executeQuery();
                
                while (rs.next()) {
                    cards.add(new Card(rs.getInt("ID"),CardType.getEnum(rs.getInt("TYPE")),ColorType.getEnum(rs.getInt("COLOR")),rs.getInt("VALUE")));
                }   
                rs.close();
            }
            Collections.shuffle(cards);
            c.close();
            System.out.println("GETCARDS: Succesfull");            
            return cards;
        } catch ( ClassNotFoundException | SQLException e ) {
            System.out.println("GETCARDS: Failed");
            return null;
        }
    }

    @Override
    public Card getCard(int type, int color, int value) throws RemoteException {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            
            Card card = null;
            try (PreparedStatement query = c.prepareStatement("SELECT * FROM CARDS WHERE TYPE == ? AND COLOR == ? AND VALUE == ?")) {
                query.setInt(1, type);
                query.setInt(2, color);
                query.setInt(3, value);
                ResultSet rs = query.executeQuery();
                while (rs.next()) {
                    card = new Card(rs.getInt("ID"),CardType.getEnum(rs.getInt("TYPE")),ColorType.getEnum(rs.getInt("COLOR")),rs.getInt("VALUE"));
                }   
                rs.close();
            }
            c.close();
            System.out.println("GETCARD: Succesfull");            
            return card;
        } catch ( ClassNotFoundException | SQLException e ) {
            System.out.println("GETCARD: Failed");
            return null;
        }
    }

    @Override
    public boolean setScore(int playerID, int gameID, int score, boolean win, boolean share) throws RemoteException {
        try {
            if(share) {
                ArrayList<Integer> dbServers = getDbPorts();
                for(Integer db : dbServers){
                    if(dbPort != db) {
                        Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, db);
                        Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
                        impl.setScore(playerID, gameID, score, win, false);
                    }
                }
            }
            
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            
            try(PreparedStatement select = c.prepareStatement("SELECT * FROM PLAYERS_GAMES WHERE GAME_ID = ? AND PLAYER_ID = ?")){
                select.setInt(1, gameID);
                select.setInt(2, playerID);
                ResultSet rs = select.executeQuery();
                
                Date rv = new Date();
                String rowversion = dateFormat.format(rv);
                Date test = new Date(rs.getString("ROWVERSION"));

                while(rs.next() && test.before(rv)){
                    try (PreparedStatement query = c.prepareStatement("UPDATE PLAYERS_GAMES SET SCORE = ?, ROWVERSION = ? WHERE GAME_ID = ? AND PLAYER_ID = ?")) {
                        query.setInt(1, score);
                        query.setString(2, rowversion);
                        query.setInt(3, gameID);
                        query.setInt(4, playerID);
                        query.executeUpdate();
                    }
                }
            }
            
            c.close();
            System.out.println("UPDATE SCORE: Succesfull");            
            return true;
        } catch ( Exception e ) {
            System.out.println("UPDATE SCORE: Failed");
            e.printStackTrace();
            return false;
        }
    }
    
    @Override
    public Player getPlayer(int playerID) throws RemoteException {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            
            Player player = null;
            try (PreparedStatement query = c.prepareStatement("SELECT * FROM PLAYERS WHERE ID == ?")) {
                query.setInt(1, playerID);
                ResultSet rs = query.executeQuery();
                while (rs.next()) {
                    player = new Player(rs.getInt("ID"), rs.getString("USERNAME"));
                }   
                rs.close();
            }
            c.close();
            System.out.println("GETPLAYER: Succesfull");            
            return player;
        } catch ( ClassNotFoundException | SQLException e ) {
            System.out.println("GETPLAYER: Failed");
            return null;
        }
    }
    
    @Override
    public List<HighScore> getHighScores() throws RemoteException {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            
            List<HighScore> highScores = new ArrayList<>();
            try (PreparedStatement query = c.prepareStatement("SELECT * FROM PLAYERS_GAMES WHERE SCORE != 0 ORDER BY SCORE DESC LIMIT 50")) {
                ResultSet rs = query.executeQuery();
                
                while (rs.next()) {                
                    int playerID = rs.getInt("PLAYER_ID");
                    int score = rs.getInt("SCORE");
                    Player p = getPlayer(playerID);
                    System.out.println(p);
                    highScores.add(new HighScore(p, score));
                }   
                rs.close();
            }
            System.out.println(highScores.size());
            c.close();
            System.out.println("GETHIGHSCORES: Succesfull");            
            return highScores;
        } catch ( ClassNotFoundException | SQLException e ) {
            System.out.println("GETHIGHSCORES: Failed");
            e.printStackTrace();
            return null;
        }
    }
}
