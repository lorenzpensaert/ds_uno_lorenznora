/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servers;

import Common.*;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.ImageIcon;

public class ApplicationImpl extends UnicastRemoteObject implements Application {
    private Map<Integer, Game> games;
    private int databasePort;
    private ImageIcon background;
       
    public ApplicationImpl(int databasePort) throws RemoteException {
        this.databasePort = databasePort;
        games = new HashMap<>();
        storeBackground();
    }
    
    private void storeBackground() {
        try {
            Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, databasePort);
            Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
            background = impl.getBackground();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public boolean register(String name, String password) throws RemoteException {
        try {
            //Connect to the database server to check username and register user
            Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, databasePort);
            Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
            return impl.register(name, password, true);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Player login(String name, String password) throws RemoteException {
        try {
            //connect to database to check credentials and login user
            Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, databasePort);
            Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
            return impl.login(name, password);
        } catch (NotBoundException | RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean isSessionTokenValid(int playerID) throws RemoteException {
        try {
            Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, databasePort);
            Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
            return impl.isSessionTokenValid(playerID);
        } catch (NotBoundException | RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean logout(int playerID) throws RemoteException {
        try {
            //connect to database to check credentials and login user
            Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, databasePort);
            Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
            return impl.logout(playerID);
        } catch (NotBoundException | RemoteException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public int createGame(String name, int players) throws RemoteException {        
        try {
            Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, databasePort);
            Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
            int gameID = impl.createGame(name, players, true);
            Game g = new Game(gameID, name, players, true);
            games.put(gameID, g);
            return gameID;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public synchronized boolean joinGame(int gameID, Player player) throws RemoteException {
        try {
            Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, databasePort);
            Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
            impl.joinGame(gameID, player.getID(), true);
            Game g = games.get(gameID);
            boolean result = g.addPlayer(player);
            notifyAll();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    @Override
    public boolean leaveGame(int gameID, int playerID) throws RemoteException {
        Game g = games.get(gameID);
        
        return g.removePlayer(playerID);
    }

    @Override
    public boolean setGameState(int gameID, boolean running) throws RemoteException {
        if(!running){
            games.remove(gameID);
            return true;
        }
        return false;
    }

    @Override
    public List<Player> getGamePlayers(int gameID) throws RemoteException {
        Game g = games.get(gameID);
        return g.getPlayers();
    }

    @Override
    public ArrayList<Game> getActiveGames() throws RemoteException {
        List<Game> lobby = new ArrayList<>();
        for(Game g : games.values()) {
            if(g.isRunning())
                lobby.add(g);
        }
        return new ArrayList<Game>(games.values());
    }

    @Override
    public synchronized boolean waitForPlayers(int gameID) throws RemoteException, InterruptedException {
        Game g = games.get(gameID);
        while(g.getPlayerCount() != g.getPlayersJoined()){
            wait();
        }
        return true;
    }
    
    @Override
    public synchronized boolean waitGameChanged(int gameID) throws RemoteException, InterruptedException {
        Game g = games.get(gameID);
        while(!g.hasGameChanged()) {
            wait();
        }
        g.setGameChanged();
        return true;
    }
    
    @Override
    public boolean checkCard(int gameID, Card card) throws RemoteException{
        Game g = games.get(gameID);
        Card lastPlayed = g.getLastPlayedCard();       
        if (lastPlayed == null || lastPlayed.getColorEnum() == ColorType.UNCOLORED) return true; // No card has been played yet OR last card was change of color, anything can be played
        if  (lastPlayed.getColor() == card.getColor() || // Kleur van kaart == Kleur van vorige kaart OF
            (lastPlayed.getType() == card.getType() && lastPlayed.getValue() == card.getValue()) || // Type van kaart EN waarde van kaart == andere kaart OF
            card.getTypeEnum() == CardType.COLOR || (card.getTypeEnum() == CardType.ADD && card.getColorEnum() == ColorType.UNCOLORED)){ // Verkleur kaart OF add + uncolored kaart EENDER 
            return true;
        }
        return false;
    }
    
    @Override
    public synchronized void playCard(int gameID, int playerID, Card card) throws RemoteException {
        Game g = games.get(gameID);
        g.playCard(databasePort, playerID, card);        
        g.getNextTurn();        
        notifyAll();
    }
    
    @Override
    public synchronized void endTurn(int gameID) throws RemoteException {
        Game g = games.get(gameID);
        g.getNextTurn();
        notifyAll();
    }

    @Override
    public synchronized Card drawCard(int gameID, int playerID) throws RemoteException {
        Game g = games.get(gameID);
        Card c = g.drawCard(databasePort, playerID);
        notifyAll();
        return c;
    }
    
    @Override
    public void drawInitialCards(int gameID, int playerID) throws RemoteException {
        Game g = games.get(gameID);
        for(int i = 0; i <= 6; i++)
            g.drawCard(databasePort, playerID);
    }
    
    @Override    
    public List<String> getPlayerCards(int gameID, int playerID) throws RemoteException {
        Game g = games.get(gameID);
        List<String> playerCards = new ArrayList<>();
        for(Card c : g.getPlayerCards(playerID)){
            playerCards.add(c.toString());
        }
        return playerCards;
    }    
    
    @Override
    public synchronized boolean waitForTurn(int gameID, int playerID) throws RemoteException, InterruptedException {
        Game g = games.get(gameID);
        while(playerID != g.getCurrentPlayer() && g.getCurrentPlayer() != -1) {
            wait();
        }
        if(g.getCurrentPlayer() == -1) return false;
        return true;        
    }
    
    @Override
    public Player getPlayer(int gameID, int playerID) throws RemoteException {
        Game g = games.get(gameID);
        return g.getPlayer(playerID);
    }

    @Override
    public List<Card> getCards() throws RemoteException {
        try {
            Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, databasePort);
            Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
            return impl.getCards();
        } catch (NotBoundException | RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public Card getCard(int type, int color, int value) throws RemoteException{
        try {
            Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, databasePort);
            Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
            return impl.getCard(type, color, value);
        } catch (NotBoundException | RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public Card getLastCard(int gameID) throws RemoteException {
        Game g = games.get(gameID);
        return g.getLastPlayedCard();
    }

    @Override
    public boolean setScore(int playerID, int gameID, int score, boolean win) throws RemoteException {
        try {
            Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, databasePort);
            Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
            Game g = games.get(gameID);
            g.getPlayer(playerID).setScore(score);
            return impl.setScore(playerID, gameID, score, win, true);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    @Override
    public List<HighScore> getHighscores() throws RemoteException {
        try {
            Registry myRegistry = LocateRegistry.getRegistry(Config.DATABASE_IP, databasePort);
            Database impl = (Database) myRegistry.lookup(Config.DATABASE_NAME);
            return impl.getHighScores();
        } catch (NotBoundException | RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @Override
    public ImageIcon getBackground() throws RemoteException {
        return background;
    }
}
