/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servers;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import Common.Config;

/**
 *
 * @author lorpo
 */
public class DispatcherServer {
    
    public void init() {
        try {
            Registry registry = LocateRegistry.createRegistry(Config.DISPATCH_PORT);
            registry.rebind(Config.DISPATCH_NAME, new DispatcherImpl());
            System.out.println("Dispatcher is ready.");
        } catch (Exception e) {
            System.out.println("Dispatcher failed to start.");
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        DispatcherServer dispatch = new DispatcherServer();
        dispatch.init();
    }
    
}
