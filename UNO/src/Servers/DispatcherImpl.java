/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servers;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import Common.Config;
import Common.Dispatcher;
import java.util.List;

/**
 *
 * @author lorenzpensaert
 */
public class DispatcherImpl extends UnicastRemoteObject implements Dispatcher {
    HashMap<Integer, ApplicationServer> appServers = new HashMap<>(); 
    ArrayList<DatabaseServer> dbServers = new ArrayList<>();
    ArrayList<Integer> dbPorts = new ArrayList<>();
    private int dbCounter = 0;
    
    public DispatcherImpl() throws RemoteException {
        for(int i = 0; i < Config.DATABASE_MAX; i++) {
            int port = Config.DATABASE_PORTBASE + i;
            dbServers.add(new DatabaseServer(port));
            dbPorts.add(port);
        }
    }
    
    @Override
    public boolean joinServer(int appPort) throws RemoteException {
        ApplicationServer app = appServers.get(appPort);
        if(app == null) return false;
        boolean joined = app.addPlayer();
        if (!joined) return false;
        if (app.isFull()) {
            appServers.put(Config.APPLICATION_PORTBASE + appServers.size(), new ApplicationServer(Config.APPLICATION_PORTBASE + appServers.size(), Config.DATABASE_PORTBASE + dbCounter));
            dbCounter++;
            if(dbCounter >= Config.DATABASE_MAX) dbCounter = 0;
        }
        return true;
    }
    
    @Override
    public void leaveServer(int appPort) throws RemoteException {
        ApplicationServer app = appServers.get(appPort);
        if(app == null) return;
        app.removePlayer();
        if (app.isEmpty() && appServers.size() > 1) {
            app.shutdown();
            appServers.remove(appPort);
        }
    }

    @Override
    public ArrayList<ApplicationServer> getServers() throws RemoteException {
        if(appServers.isEmpty()) {
            appServers.put(Config.APPLICATION_PORTBASE + appServers.size(), new ApplicationServer(Config.APPLICATION_PORTBASE + appServers.size(), Config.DATABASE_PORTBASE + dbCounter));
            dbCounter++;
           if(dbCounter >= Config.DATABASE_MAX) dbCounter = 0;
        }
        return new ArrayList(appServers.values());
    }
    
    @Override
    public ArrayList<Integer> getDbPorts() throws RemoteException {
        return this.dbPorts;
    }
}
