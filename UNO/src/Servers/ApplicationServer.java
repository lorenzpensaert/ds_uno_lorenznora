/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servers;

import java.io.Serializable;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import Common.Config;

public class ApplicationServer implements Serializable {
    private int port;
    private int connections;
    private int databasePort;
    private Registry registry;
    
    public ApplicationServer(int port, int databasePort) {
        this.port = port;
        this.databasePort = databasePort;
        start();
    }
    
    public int getPort() {
        return port;
    }
    
    public boolean isFull () {
        return connections == Config.APPLICATION_MAX_PLAYERS;
    }
    
    public boolean isEmpty () {
        return connections == 0;
    }
    
    private void start() {
        try {
            registry = LocateRegistry.createRegistry(port);
            registry.rebind(Config.APPLICATION_NAME, new ApplicationImpl(databasePort));
            System.out.println("Application server is ready.");
        } catch (Exception e) {
            System.out.println("Application server failed to start.");
        }
    }
    
    public void shutdown() {
        try {
            registry.unbind(Config.APPLICATION_NAME);
        } catch (Exception e) {
            System.out.println("Application server failed to shutdown");
        }
    }

    @Override
    public String toString() {
        return "Server " + (port - Config.APPLICATION_PORTBASE) + " | Players: " + connections + "/20";
    }
    
    public boolean addPlayer() {
        if(connections + 1 <= Config.APPLICATION_MAX_PLAYERS) {
            connections++;
            return true;
        } 
        return false;
    }
    
    public boolean removePlayer() {
        if(connections - 1 >= 0) {
            connections--;
            return true;
        }
        return false;
    }
}
